module com.RuslanShakhrai {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires java.desktop;

    opens com.RuslanShakhrai to javafx.fxml, javafx.controls,javafx.base,java.desktop;
    exports com.RuslanShakhrai;
}