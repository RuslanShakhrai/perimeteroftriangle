package com.RuslanShakhrai;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("primary.fxml"));
        Image ico = new Image(new FileInputStream("src/main/resources/com/RuslanShakhrai/images/design-tool.png"));
        stage.getIcons().add(ico);
        stage.setTitle("Определение периметра треугольника");
        stage.setScene(new Scene(root, 486, 234));
        stage.setResizable(false);
        stage.show();
    }
    public static void main(String[] args) {
        launch();
    }

}