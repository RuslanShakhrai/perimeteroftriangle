package com.RuslanShakhrai;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.RuslanShakhrai.Model.Point;
import com.RuslanShakhrai.Model.Point3D;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class Controller implements Initializable {
    private ObservableList<String> dimensions = FXCollections.observableArrayList("2D", "3D");
    private double perimeter = 0;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label Label_A;

    @FXML
    private Label Label_B;

    @FXML
    private Label Label_C;

    @FXML
    private TextField X1;

    @FXML
    private TextField Y1;

    @FXML
    private TextField Z1;

    @FXML
    private TextField X2;

    @FXML
    private TextField Y2;

    @FXML
    private TextField Z2;

    @FXML
    private TextField X3;

    @FXML
    private TextField Y3;

    @FXML
    private TextField Z3;

    @FXML
    private ChoiceBox<String> DimensionBox;
    //подтверждение выхода
    @FXML
    void ExitConfirm(ActionEvent event) {
        Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
        exitAlert.setTitle("Подтверждение действия");
        exitAlert.setHeaderText("Вы действительно хотите выйти?");
        Optional<ButtonType> option = exitAlert.showAndWait();
        if (option.get().equals(ButtonType.OK)) {
            System.exit(0);
        }
    }
    //метод,позволяющий вычислить значение периметра построенного треугольника
    @FXML
    void getPerimeter(ActionEvent event) {
        if (getDimension().equals(dimensions.get(0))) {
            try {
                Point pointA = new Point(Double.parseDouble(X1.getText()), Double.parseDouble(Y1.getText()));
                Point pointB = new Point(Double.parseDouble(X2.getText()), Double.parseDouble(Y2.getText()));
                Point pointC = new Point(Double.parseDouble(X3.getText()), Double.parseDouble(Y3.getText()));
                boolean equalsPoints = equalsPoints(pointA, pointB, pointC);
                Point VectorAB = new Point(pointB.getX() - pointA.getX(), pointB.getY() - pointA.getY());
                Point VectorBC = new Point(pointC.getX() - pointB.getX(), pointC.getY() - pointB.getY());
                Point VectorAC = new Point(pointC.getX() - pointA.getX(), pointC.getY() - pointA.getY());
                double lengthOfAB = Math.sqrt(Math.pow(VectorAB.getX(), 2) + Math.pow(VectorAB.getY(), 2));
                double lengthOfBC = Math.sqrt(Math.pow(VectorBC.getX(), 2) + Math.pow(VectorBC.getY(), 2));
                double lengthOfAC = Math.sqrt(Math.pow(VectorAC.getX(), 2) + Math.pow(VectorAC.getY(), 2));
                boolean equationOfTriangle = equationOfTriangle(lengthOfAB,lengthOfBC,lengthOfAC);
                if(equalsPoints == false && equationOfTriangle == true){//если точки не равны и выполняется неравенство треугольника
                    saveData(pointA,pointB,pointC);
                    resetInputs();
                }
            } catch (NumberFormatException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Неверно введенные данные");
                alert.setContentText("Числа должны иметь либо целочисленный, либо вещественный тип данных");
                alert.showAndWait();
            }
        } else {
            try {
                Point3D pointA = new Point3D(Double.parseDouble(X1.getText()), Double.parseDouble(Y1.getText()), Double.parseDouble(Z1.getText()));
                Point3D pointB = new Point3D(Double.parseDouble(X2.getText()), Double.parseDouble(Y2.getText()), Double.parseDouble(Z2.getText()));
                Point3D pointC = new Point3D(Double.parseDouble(X3.getText()), Double.parseDouble(Y3.getText()), Double.parseDouble(Z3.getText()));
                boolean equalsPoints = equalsPoints(pointA, pointB, pointC);
                Point3D VectorAB = new Point3D(pointB.getX() - pointA.getX(), pointB.getY() - pointA.getY(), pointB.getZ() - pointA.getZ());
                Point3D VectorBC = new Point3D(pointC.getX() - pointB.getX(), pointC.getY() - pointB.getY(), pointC.getZ() - pointB.getZ());
                Point3D VectorAC = new Point3D(pointC.getX() - pointA.getX(), pointC.getY() - pointA.getY(), pointC.getZ() - pointA.getZ());
                double lengthOfAB = Math.sqrt(Math.pow(VectorAB.getX(), 2) + Math.pow(VectorAB.getY(), 2) + Math.pow(VectorAB.getZ(), 2));
                double lengthOfBC = Math.sqrt(Math.pow(VectorBC.getX(), 2) + Math.pow(VectorBC.getY(), 2) + Math.pow(VectorBC.getZ(), 2));
                double lengthOfAC = Math.sqrt(Math.pow(VectorAC.getX(), 2) + Math.pow(VectorAC.getY(), 2) + Math.pow(VectorAC.getZ(), 2));
                boolean equationOfTriangle = equationOfTriangle(lengthOfAB,lengthOfBC,lengthOfAC);
                if(equalsPoints == false && equationOfTriangle == true){//если точки не равны и выполняется неравенство треугольника
                    saveData(pointA,pointB,pointC);
                    resetInputs();
                }
            } catch (NumberFormatException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Неверно введенные данные");
                alert.setContentText("Числа должны иметь либо целочисленный, либо вещественный тип данных");
                alert.showAndWait();
            }
        }
    }
    //метод, выполняющийся при инициализации
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setItemsForDimensionBox();
        hideInputs();
        getDimension();
    }
    //метод, устанавливающий значение для ComboBox
    public void setItemsForDimensionBox() {
        DimensionBox.setItems(dimensions);
        DimensionBox.setValue(dimensions.get(0));
    }
    //метод, скрывающие лишние поля для ввода
    public void hideInputs() {
        Z1.setVisible(false);
        Z2.setVisible(false);
        Z3.setVisible(false);
    }
    //метод, возвращающий видимость для полей ввода
    public void showInputs() {
        Z1.setVisible(true);
        Z2.setVisible(true);
        Z3.setVisible(true);
    }
    //метод, позволяющий переключать систему координат (2D или 3D)
    public String getDimension() {
        DimensionBox.setOnAction(actionEvent -> {
            if (DimensionBox.getValue().equals(dimensions.get(0))) {
                hideInputs();
            } else {
                showInputs();
            }
        });
        return DimensionBox.getValue();
    }
    //метод, проверяющий равенство точек
    public boolean equalsPoints(Point pointA, Point pointB, Point pointC) {
        if (pointA.equals(pointB) || pointA.equals(pointC) || pointB.equals(pointC)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setTitle("Неверно введенные данные");
            alert.setContentText("Введенные вами точки совпадают, поэтому треугольник построить невозможно");
            alert.show();
            return true;
        }
        return false;
    }
    //метод, проверяющий неравенство треугольника
    public boolean equationOfTriangle(double lengthOfAB, double lengthOfBC, double lengthOfAC) {
        if (lengthOfAB + lengthOfBC <= lengthOfAC || lengthOfAC + lengthOfBC <= lengthOfAB || lengthOfAB + lengthOfAC <= lengthOfBC) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Неверно введенные данные");
            alert.setContentText("Из введенных вами точек невозможно построить треугольник в силу свойства о неравенстве треугольника");
            alert.showAndWait();
            System.out.println(lengthOfAB);
            System.out.println(lengthOfBC);
            System.out.println(lengthOfAC);
            return false;
        } else {
            perimeter = lengthOfAB + lengthOfAC + lengthOfBC;
            Alert resultAlert = new Alert(Alert.AlertType.INFORMATION);
            resultAlert.setTitle("Успешно");
            resultAlert.setHeaderText("Введенные вами данные были успешно обработаны и сохранены в файл");
            resultAlert.setContentText("Периметр построеного треугольника равен: " + perimeter + " ед.");
            resultAlert.showAndWait();
            System.out.println(lengthOfAB);
            System.out.println(lengthOfBC);
            System.out.println(lengthOfAC);
        }
        return true;
    }
    //метод, сохраняющий результат в файл 2D
    public void saveData(Point pointA,Point pointB,Point pointC){
        try {
            FileWriter fileWriter = new FileWriter("data2D.txt",true);
            fileWriter.write("Периметр треугольника, построенного на координатах: "+"x1 = "+pointA.getX()
                    +" y1 = "+pointA.getY()+" x2 ="+pointB.getX()+" y2 = "+pointB.getY()+" x3 = "+pointC.getX()+" y3 = "+pointC.getY()+" равен: "+perimeter+ " ед. \n");
            fileWriter.flush();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
    //метод, сохраняющий результат в файл 3D
    public void saveData(Point3D pointA,Point3D pointB,Point3D pointC){
        try {
            FileWriter fileWriter = new FileWriter("data3D.txt",true);
            fileWriter.write("Периметр треугольника, построенного на координатах: "+"x1 = "+pointA.getX()
                    +" y1 = "+pointA.getY()+" z1 ="+pointA.getZ()+" x1 = "+pointB.getX()+" y2 = "+pointB.getY()+" z2 = "+pointB.getZ()+" x3 = "+pointC.getX()
            +" y3 = "+pointC.getY()+" z3 = "+pointC.getZ()+" равен: "+ perimeter+" ед. \n");
            fileWriter.flush();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
    //метод для сброса полей для ввода
    public void resetInputs(){
        X1.setText("");
        X2.setText("");
        X3.setText("");
        Y1.setText("");
        Y2.setText("");
        Y3.setText("");
        Z1.setText("");
        Z2.setText("");
        Z3.setText("");
    }
}

